<?php

//$sets = distributeToSets([1, 2, 1]);
//
//var_dump($sets);

function distributeToSets(array $input): array
{

    $sets = [];
    foreach ($input as $each) {
        if (array_key_exists($each, $sets)) {

            // same as array_push($sets[$each], $each);
            $sets[$each][] = $each;

        } else {
            $sets[$each] = [$each];
        }
    }
    return $sets;
}
