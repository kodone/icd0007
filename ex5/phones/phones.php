<?php

require_once __DIR__ . '/../connection.php';
require_once __DIR__ . '/Contact.php';

$contacts = getContacts();

foreach ($contacts as $contact) {
    print $contact;
}

function getContacts(): array
{
    $conn = getConnection();

    $stmt = $conn->prepare('SELECT id, name, number 
                                 from contact c
                                 left join phone p on c.id = p.contact_id');

    $stmt->execute();
    $contacts = [];
    foreach ($stmt as $row) {
        $id = $row['id'];
        $number = $row['number'];
        $name = $row['name'];


        if (array_key_exists($id, $contacts)) {
            $contact = $contacts[$id];

        } else {
            $contact = new Contact($id, $name);
            $contacts[$id] = $contact;
        }

        if ($number !== null) {
            $contact->addPhone($number);
        }


    }

    //converts dict to array
    return array_values($contacts);
}