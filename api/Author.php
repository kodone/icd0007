<?php

class Author
{
    public string $firstName;
    public string $lastName;
    public string $grade;
    public string $id;

    public function __construct(string $firstName, string $lastName, $grade, $id){
        $this ->firstName = $firstName;
        $this ->lastName = $lastName;
        $this ->grade = $grade;
        $this ->id = $id;
    }

    public function getFullName(): string
    {
        return $this->firstName . ' ' . $this->lastName;
    }

    public function __toString() : string
    {
        return sprintf('First name: %s, last name: %s, grade: %s, with ID: %s',
            $this ->firstName, $this ->lastName, $this ->grade, $this->id);
    }
}