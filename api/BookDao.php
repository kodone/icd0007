<?php

//include_once __DIR__ . '/Book.php';
require_once('database-connection.php');
require_once('book-actions.php');
require_once ('AuthorDao.php');
require_once('Book.php');

class BookDao
{

    public function getBooks(): array
    {

        $connection = getConnection();
        $statement = $connection->prepare('
    SELECT title, book_grade, is_read, book.book_id, first_name, last_name, author_grade, author.author_id
    FROM book
    LEFT JOIN book_author ON book.book_id = book_author.book_id
    LEFT JOIN author ON book_author.author_id = author.author_id
    ORDER BY book_id;');
        $statement->execute();
        $connection = null;

        $books = [];
        if ($statement->rowCount() > 0) {
            foreach ($statement as $row) {
                $bookId = $row['book_id'];
                //if book already exists
                if (containsBookWithId($books, $bookId)) {
                    $alreadyExistingBook = getBookByIdFromBookCollection($books, $bookId);
                    if (isset($row['author_id'])) {
                        $firstName = $row['first_name'];
                        $lastName = $row['last_name'];
                        $authorGrade = $row['author_grade'];
                        $authorId = $row['author_id'];

                        $authorObj = new Author($firstName, $lastName, $authorGrade, $authorId);
                        $alreadyExistingBook->addBookAuthor($authorObj);
                    }
                    //if this is brand-new book
                } else {
                    $title = $row['title'];
                    $bookGrade = $row['book_grade'];
                    $isRead = $row['is_read'];
                    $book = new Book($title, $bookGrade, $isRead, $bookId);

                    if (isset($row['author_id'])) {
                        $firstName = $row['first_name'];
                        $lastName = $row['last_name'];
                        $authorGrade = $row['author_grade'];
                        $authorId = $row['author_id'];

                        $authorObj = new Author($firstName, $lastName, $authorGrade, $authorId);
                        $book->addBookAuthor($authorObj);
                    }
                    array_push($books, $book);
                }
            }
        }
        return $books;

    }

    public function addBook($bookDto, $authorsId)
    {
        if (!empty($bookDto->id)) {
            $this->editBook($bookDto, $authorsId);

        } else {
            $title = $bookDto->title;
            $grade = $bookDto->grade;
            $isRead = $bookDto->isRead;

            $connection = getConnection();
            $statement = $connection->prepare(
                'INSERT INTO book(title, book_grade, is_read) VALUES (:title, :book_grade, :is_read)');

            $statement->bindValue(':title', $title);
            $statement->bindValue(':book_grade', intval($grade));
            //TODO:  mb needs to be converted from string to bool
            $statement->bindValue(':is_read', intval($isRead));

            $statement->execute();

            if (!empty($authorsId)) {
                $authorDao = new AuthorDao();
                $bookId = $connection->lastInsertId();

                foreach ($authorsId as $authorID) {
                    //defence from "garbage" ids, like id = " ' '" or id = " 'd' "
                    $authorsObjects = $authorDao->getAuthors();
                    foreach ($authorsObjects as $auth) {
                        if ($auth->id == intval($authorID)) {
                            $statement = $connection->prepare(
                                'INSERT INTO book_author(book_id, author_id) VALUES (:book_id, :author_id)');

                            $statement->bindValue(':book_id', intval($bookId));
                            $statement->bindValue(':author_id', intval($authorID));
                            $statement->execute();
                        }
                    }
                }
            }
            $connection = null;
        }
    }

    function editBook($bookDto, $authorsId)
    {
        $title = $bookDto->title;
        $grade = intval($bookDto->grade);
        $isRead = intval($bookDto->isRead);
        $id = intval($bookDto->id);

        $connection = getConnection();
        $statement = $connection->prepare('
        UPDATE book
        SET title = :title, book_grade = :book_grade, is_read = :is_read
        WHERE book_id = :book_id');

        $statement->bindValue(':title', $title);
        $statement->bindValue(':book_grade', $grade);
        $statement->bindValue(':is_read', $isRead);
        $statement->bindValue(':book_id', $id);
        $statement->execute();

        $statement = $connection->prepare('
            DELETE FROM book_author WHERE book_id = :book_id');
        $statement->bindValue(':book_id', $id);
        $statement->execute();

        if (!empty($authorsId)) {
            $statement = $connection->prepare('
            DELETE FROM book_author WHERE book_id = :book_id');
            $statement->bindValue(':book_id', $id);
            $statement->execute();
            error_log(print_r($authorsId, true));

            $authorDao = new AuthorDao();
            $authorsObjects = $authorDao->getAuthors();
            foreach ($authorsId as $authorID) {
                //defence from "garbage" ids, like id = " ' '" or id = " 'd' "
                foreach ($authorsObjects as $auth){
                    if ($auth->id == intval($authorID)){
                        $statement = $connection->prepare(
                            'INSERT INTO book_author(book_id, author_id) VALUES (:book_id, :author_id)');

                        $statement->bindValue(':book_id', $id);
                        $statement->bindValue(':author_id', intval($authorID));
                        $statement->execute();
                    }
                }

            }
        }
        $connection = null;
    }

    public function getBookById($id)
    {
        $books = $this->getBooks();
        foreach ($books as $book) {
            if ($id == $book->id) {
                return $book;
            }
        }
        return null;
    }

    public function deleteBookById($idToDelete)
    {
        $connection = getConnection();
        $statement = $connection->prepare('DELETE FROM book WHERE book_id = :book_id');

        $statement->bindValue(':book_id', intval($idToDelete));

        $statement->execute();
        $connection = null;
    }

}