<?php
require_once ('Book.php');
require_once ('Author.php');

function echoJson($object){
    $json = json_encode($object, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE);
    header('Content-Type: application/json');
    echo $json;
}

function getBookAuthorsIdFromAssocArray($assocArray): array
{
    $authorsId = [];
    if (isset($assocArray['firstAuthorId']) && !empty($assocArray['firstAuthorId'])){
        $firstId = intval($assocArray['firstAuthorId']);
        $authorsId[] = $firstId;
    }
    if (isset($assocArray['secondAuthorId']) && !empty($assocArray['secondAuthorId'])){
        $secondId = intval($assocArray['secondAuthorId']);
        $authorsId[] = $secondId;
    }
    return $authorsId;
}