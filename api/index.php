<?php
require_once('BookDao.php');
require_once('AuthorDao.php');
require_once('Book.php');
require_once('Author.php');
require_once('functions.php');

$bookDao = new BookDao();
$authorDao = new AuthorDao();
$command = 'default';

if (isset($_GET["command"])) {
    $command = $_GET["command"];
}

if ($command === 'getBooks') {
    $books = $bookDao->getBooks();
    echoJson($books);

} else if ($command === 'getBook') {
    $bookId = intval($_GET['id']);
    $book = $bookDao->getBookById($bookId);

    if ($book == null) {
        http_response_code(404);
    } else {
        echoJson($book);
    }

} else if ($command === 'deleteBook') {
    $bookId = intval($_GET['bookId']);
    $bookDao->deleteBookById($bookId);
    http_response_code(205);

} else if ($command === 'addBook') {
    $bookJson = file_get_contents("php://input");
    $bookData = json_decode($bookJson, true);

    $bookTitle = $bookData['title'];
    $bookGrade = $bookData['grade'];
    $bookIsRead = $bookData['isRead'];
    $authorsId = getBookAuthorsIdFromAssocArray($bookData);
    $bookObject = new Book($bookTitle, $bookGrade, $bookIsRead, '');

    $bookDao->addBook($bookObject, $authorsId);
    http_response_code(201);

} else if ($command === 'editBook') {
    $bookJson = file_get_contents("php://input");
    $bookData = json_decode($bookJson, true);

    $bookTitle = $bookData['title'];
    $bookGrade = $bookData['grade'];
    $bookIsRead = $bookData['isRead'];
    $bookId = $bookData['id'];
    $bookObject = new Book($bookTitle, $bookGrade, $bookIsRead, $bookId);
    $authorsId = getBookAuthorsIdFromAssocArray($bookData);

    $bookDao->editBook($bookObject, $authorsId);
    http_response_code(205);
} else if ($command === 'getAuthors') {
    $authors = $authorDao->getAuthors();
    echoJson($authors);

} else if($command === 'getBookById'){
    $bookId = intval($_REQUEST['bookId']);
    $bookObj = $bookDao->getBookById($bookId);

    if ($bookObj == null) {
        http_response_code(404);
    } else {
        echoJson($bookObj);
    }
}