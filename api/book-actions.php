<?php

require_once('database-connection.php');

function addBook($title, $grade, $isRead, $id, $authorsId)
{
    if (is_null($id)) {
        $connection = getConnection();
        $statement = $connection->prepare(
            'INSERT INTO book(title, book_grade, is_read) VALUES (:title, :book_grade, :is_read)');

        $statement->bindValue(':title', $title);
        $statement->bindValue(':book_grade', intval($grade));
        //TODO:  mb needs to be converted from string to bool
        $statement->bindValue(':is_read', intval($isRead));

        $statement->execute();
        $bookId = $connection->lastInsertId();

        if (isset($authorsId) && !empty($authorsId)) {
            foreach ($authorsId as $authorID) {

                //defence from "garbage" ids, like id = " ' '" or id = " 'd' "
                $authorsObjects = getAuthors();
                foreach ($authorsObjects as $auth){
                    if ($auth->id == intval($authorID)){
                        $statement = $connection->prepare(
                            'INSERT INTO book_author(book_id, author_id) VALUES (:book_id, :author_id)');


                        $statement->bindValue(':book_id', intval($bookId));
                        $statement->bindValue(':author_id', intval($authorID));
                        $statement->execute();
                    }
                }
            }
        }
        $connection = null;

    } else {
        $connection = getConnection();
        $statement = $connection->prepare('
        UPDATE book
        SET title = :title, book_grade = :book_grade, is_read = :is_read
        WHERE book_id = :book_id');

        $statement->bindValue(':title', $title);
        $statement->bindValue(':book_grade', intval($grade));
        $statement->bindValue(':is_read', $isRead);
        $statement->bindValue(':book_id', $id);

        $statement->execute();

        if (isset($authorsId)) {
            $statement = $connection->prepare('
            DELETE FROM book_author WHERE book_id = :book_id');
            $statement->bindValue(':book_id', $id);
            $statement->execute();
            foreach ($authorsId as $authorID) {
                $statement = $connection->prepare(
                    'INSERT INTO book_author(book_id, author_id) VALUES (:book_id, :author_id)');

                $statement->bindValue(':book_id', $id);
                $statement->bindValue(':author_id', intval($authorID));
                $statement->execute();
            }
        }
        $connection = null;
    }


}

function getBooks(): array
{

    $connection = getConnection();
    $statement = $connection->prepare('
    SELECT title, book_grade, is_read, book.book_id, first_name, last_name, author_grade, author.author_id
    FROM book
    LEFT JOIN book_author ON book.book_id = book_author.book_id
    LEFT JOIN author ON book_author.author_id = author.author_id
    ORDER BY book_id;');
    $statement->execute();
    $connection = null;

    $books = [];
    if ($statement->rowCount() > 0) {
        foreach ($statement as $row) {
            $bookId = $row['book_id'];
            //if book already exists
            if (containsBookWithId($books, $bookId)) {
                $alreadyExistingBook = getBookByIdFromBookCollection($books, $bookId);
                if (isset($row['author_id'])) {
                    $firstName = $row['first_name'];
                    $lastName = $row['last_name'];
                    $authorGrade = $row['author_grade'];
                    $authorId = $row['author_id'];

                    $authorObj = new Author($firstName, $lastName, $authorGrade, $authorId);
                    $alreadyExistingBook->addBookAuthor($authorObj);
                }
                //if this is brand-new book
            } else {
                $title = $row['title'];
                $bookGrade = $row['book_grade'];
                $isRead = $row['is_read'];
                $book = new Book($title, $bookGrade, $isRead, $bookId);

                if (isset($row['author_id'])) {
                    $firstName = $row['first_name'];
                    $lastName = $row['last_name'];
                    $authorGrade = $row['author_grade'];
                    $authorId = $row['author_id'];

                    $authorObj = new Author($firstName, $lastName, $authorGrade, $authorId);
                    $book->addBookAuthor($authorObj);
                }
                array_push($books, $book);
            }
        }
    }
    return $books;

}

function getBookByIdFromBookCollection($bookObjectsCollection, $bookId)
{
    foreach ($bookObjectsCollection as $book) {
        if ($book->id == $bookId) {
            return $book;
        }
    }
    return null;
}

function containsBookWithId($booksObjectsCollection, $bookId): bool
{
    foreach ($booksObjectsCollection as $book) {
        if ($book->id == $bookId) {
            return true;
        }
    }
    return false;
}

function transformDbBookToBookObject($dbRow): Book
{
    $id = $dbRow['book_id'];
    $title = $dbRow['title'];
    $grade = $dbRow['book_grade'];
    $isRead = $dbRow['is_read'];

    return new Book($title, $grade, $isRead, $id);
}

function getBookObjectById($id)
{
    $books = getBooks();

    foreach ($books as $book) {
        if ($book->id == $id) {
//            return new Book($book->title, $book->grade, $book->isRead, $book->id);
            return $book;
        }
    }
    return null;
}

function isBookTitleValid($title): bool
{
    if (empty($title)) {
        return false;
    }
    return strlen($title) >= 3 && strlen($title) <= 23;
}

function isBookDtoObjectValid($bookDto){
    if (empty($bookDto->title)) {
        return false;
    }
    return strlen($bookDto->title) >= 3 && strlen($bookDto->title) <= 23;
}

function getNewBookId(): string
{

    $contents = file_get_contents(BOOKS_ID_FILE);

    $id = intval($contents);
    file_put_contents(BOOKS_ID_FILE, $id + 1);

    return strval($id);
}

function deleteBookById($idToDelete)
{
    $connection = getConnection();
    $statement = $connection->prepare('DELETE FROM book WHERE book_id = :book_id');

    $statement->bindValue(':book_id', intval($idToDelete));

    $statement->execute();
    $connection = null;
}

function getBookAuthorsId($bookId): array
{
    $connection = getConnection();
    $statement = $connection->prepare('
    SELECT * FROM book_author WHERE book_id = :book_id');

    $statement->bindValue(':book_id', intval($bookId));
    $statement->execute();

    $authorsId = [];
    foreach ($statement as $row) {
        array_push($authorsId, $row['author_id']);
    }
    return $authorsId;
}

