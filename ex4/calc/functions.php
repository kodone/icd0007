<?php

function evaluate($expression) : int {
    if (preg_match('/[^\d +-]/', $expression, $matches)) {
        throw new RuntimeException(
            'expression contains illegal character: ' . $matches[0]);
    }

    try {
        $result = '';
        while (checkForDoubleMinus($expression) === true){
            $expression = getRidOfDoubledMinus($expression);
        }

        eval(sprintf('$result = %s;', $expression));

        return intval($result);

    } catch (Error $ex) {
        throw new RuntimeException('bad expression: ' . $expression);
    }
}

function checkForDoubleMinus($expression): bool{
    return strpos($expression, "--");
}

function getRidOfDoubledMinus($expression): string {
    $index = strpos($expression, "--");
    return substr_replace($expression, "+", $index, 2);

}