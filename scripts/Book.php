<?php

class Book
{
    public string $title;
    public string $grade;
    public string $isRead;
    public string $id;
    public array $authors = [];

    public function __construct(string $title, string $grade, string $isRead, $id){
        $this ->title = $title;
        $this ->grade = $grade;
        $this ->isRead = $isRead;
        $this ->id = $id;
    }

    public function addBookAuthor($authorObject){
        array_push($this->authors, $authorObject);
    }

    public function __toString() : string
    {
        return sprintf('Title: %s, grade: %s, is read: %s, with ID: %s',
            $this ->title, $this ->grade, $this ->isRead, $this->id);
    }

    public function getAuthorsNames(): string
    {
        $fullNames = [];
        foreach ($this->authors as $author){
            $name = $author->firstName . " " . $author->lastName;
            array_push($fullNames, $name);
        }
        return implode(", ", $fullNames);
    }
}