<?php

include_once __DIR__ . '/Author.php';
require_once('database-connection.php');
require_once ('Author.php');

class AuthorDao
{
    public function getAuthors(): array
    {

        $connection = getConnection();
        $statement = $connection->prepare('SELECT * FROM author;');
        $statement->execute();
        $connection = null;

        $authors = [];
        if ($statement->rowCount() > 0) {
            foreach ($statement as $row) {
                array_push($authors, $this->transformDbAuthorToAuthorObject($row));
            }
        }
        return $authors;
    }

    function transformDbAuthorToAuthorObject($dbRow): Author
    {
        $id = $dbRow['author_id'];
        $firstName = $dbRow['first_name'];
        $lastName = $dbRow['last_name'];
        $grade = $dbRow['author_grade'];

        return new Author($firstName, $lastName, $grade, $id);
    }

    public function addAuthor($authorDto){
        if (!empty($authorDto->id)){
            $this->editAuthor($authorDto);
        } else {
            $firstName = $authorDto->firstName;
            $lastName = $authorDto->lastName;
            $grade = intval($authorDto->grade);


            $connection = getConnection();
            $statement = $connection->prepare(
                'INSERT INTO author(first_name, last_name, author_grade) VALUES (:first_name, :last_name, :author_grade)');

            $statement->bindValue(':first_name', $firstName);
            $statement->bindValue(':last_name', $lastName);
            $statement->bindValue(':author_grade', $grade);

            $statement->execute();
            $connection = null;
        }
    }

    function editAuthor($authorDto){
        $firstName = $authorDto->firstName;
        $lastName = $authorDto->lastName;
        $grade = intval($authorDto->grade);
        $id = intval($authorDto->id);

        $connection = getConnection();
        $statement = $connection->prepare(
            'UPDATE author 
                  SET first_name = :first_name, last_name = :last_name, author_grade = :author_grade
                  WHERE author_id = :author_id');

        $statement->bindValue(':first_name', $firstName);
        $statement->bindValue(':last_name', $lastName);
        $statement->bindValue(':author_grade', $grade);
        $statement->bindValue(':author_id', $id);

        $statement->execute();
        $connection = null;
    }

    public function getAuthorById($id){

        $authors = $this->getAuthors();
        foreach ($authors as $author) {
            if ($author->id == $id) {
                return $author;
            }
        }
        return null;
    }

    public function deleteAuthorById($idToDelete){
        $connection = getConnection();
        $statement = $connection->prepare('DELETE FROM author WHERE author_id = :author_id');

        $statement->bindValue(':author_id', intval($idToDelete));

        $statement->execute();
        $connection=null;
    }
}