USE kodone;

CREATE TABLE book
(
    book_id INT AUTO_INCREMENT,
    title   VARCHAR(50),
    grade   INT,
    is_read BOOL,
    PRIMARY KEY (book_id)
);

CREATE TABLE author
(
    author_id  INT AUTO_INCREMENT,
    first_name VARCHAR(50),
    last_name  VARCHAR(50),
    grade      INT,
    PRIMARY KEY (author_id)
);

CREATE TABLE book_author
(
    book_author_id INT AUTO_INCREMENT,
    book_id        INT,
    author_id      INT,
    PRIMARY KEY (book_author_id),
    FOREIGN KEY (book_id) REFERENCES book (book_id) ON DELETE CASCADE,
    FOREIGN KEY (author_id) REFERENCES author (author_id) ON DELETE CASCADE
);

ALTER TABLE author CHANGE grade author_grade INT;
ALTER TABLE book CHANGE grade book_grade INT;