<?php
include_once __DIR__ . '/Author.php';
const AUTHORS_FILE = "text-data/authors.txt";
const AUTHORS_ID_FILE = "text-data/authors-id.txt";
require_once('database-connection.php');

function addAuthor($firstName, $lastName, $grade, $id)
{
    if (is_null($id)) {

        $connection = getConnection();
        $statement = $connection->prepare(
            'INSERT INTO author(first_name, last_name, author_grade) VALUES (:first_name, :last_name, :author_grade)');

        $statement->bindValue(':first_name', $firstName);
        $statement->bindValue(':last_name', $lastName);
        $statement->bindValue(':author_grade', intval($grade));

        $statement->execute();
        $connection = null;

    } else {

        $connection = getConnection();
        $statement = $connection->prepare(
            'UPDATE author 
                  SET first_name = :first_name, last_name = :last_name, author_grade = :author_grade
                  WHERE author_id = :author_id');

        $statement->bindValue(':first_name', $firstName);
        $statement->bindValue(':last_name', $lastName);
        $statement->bindValue(':author_grade', intval($grade));
        $statement->bindValue(':author_id', $id);

        $statement->execute();
        $connection = null;

    }

}

function getAuthors(): array
{

    $connection = getConnection();
    $statement = $connection->prepare('SELECT * FROM author;');
    $statement->execute();
    $connection = null;

    $authors = [];
    if ($statement->rowCount() > 0) {
        foreach ($statement as $row) {
            array_push($authors, transformDbAuthorToAuthorObject($row));
        }
    }
    return $authors;
}

function getAuthorObjectById($id)
{

    $authors = getAuthors();

    foreach ($authors as $author) {
        if ($author->id == $id) {
            return $author;
        }
    }
    return null;
}

function isAuthorNameValid($firstName, $lastName): bool
{
    if (empty($firstName) && empty($lastName)) {
        return false;
    }
    $isFirstNameValid = strlen($firstName) >= 1 && strlen($firstName) <= 21;
    $isLastNameValid = strlen($lastName) >= 2 && strlen($lastName) <= 22;
    return $isFirstNameValid && $isLastNameValid;
}

function isAuthorDtoObjectValid($author): bool
{
    if (empty($author->firstName) && empty($author->lastName)) {
        return false;
    }
    $isFirstNameValid = strlen($author->firstName) >= 1 && strlen($author->firstName) <= 21;
    $isLastNameValid = strlen($author->lastName) >= 2 && strlen($author->lastName) <= 22;
    return $isFirstNameValid && $isLastNameValid;
}

function getNewAuthorId(): string
{

    $contents = file_get_contents(AUTHORS_ID_FILE);

    $id = intval($contents);
    file_put_contents(AUTHORS_ID_FILE, $id + 1);

    return strval($id);
}

function deleteAuthorById($idToDelete)
{
    $connection = getConnection();
    $statement = $connection->prepare('DELETE FROM author WHERE author_id = :author_id');

    $statement->bindValue(':author_id', intval($idToDelete));

    $statement->execute();
    $connection=null;
}

function transformDbAuthorToAuthorObject($dbRow): Author
{
    $id = $dbRow['author_id'];
    $firstName = $dbRow['first_name'];
    $lastName = $dbRow['last_name'];
    $grade = $dbRow['author_grade'];

    return new Author($firstName, $lastName, $grade, $id);
}