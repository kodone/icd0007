const books = async (params) => {
    document.title = "Raamatud";
    const books = await fetch('/api/index.php?command=getBooks')
        .then(response => response.json());

    let page = document.createElement("div");
    let table = document.createElement("div");
    table.classList.add('div-table');

    let tableHeading = document.createElement('div');
    tableHeading.classList.add('div-table-heading');

    let tableHeadingRow = document.createElement('div');
    tableHeadingRow.classList.add('div-table-row');

    const titleHeading = document.createElement('div');
    titleHeading.classList.add('div-table-head');
    titleHeading.textContent = 'Pealkiri';

    const authorsHeading = document.createElement('div');
    authorsHeading.classList.add('div-table-head');
    authorsHeading.textContent = 'Autorid';

    const gradesHeading = document.createElement('div');
    gradesHeading.classList.add('div-table-head');
    gradesHeading.textContent = 'Hinne';

    books.forEach( book => {
        const row = document.createElement('div');
        row.classList.add('div-table-row');

        const title = document.createElement('div');
        title.classList.add('div-table-cell');
        const link = document.createElement('a');
        link.textContent = book.title;
        link.setAttribute('inner-link', '');
        link.href = `/add-book?bookId=${book.id}`;
        title.appendChild(link);

        const authors = document.createElement('div');
        authors.classList.add('div-table-cell');
        if (book.authors.length > 0){
            let authorsString = '';
            book.authors.forEach(author => {
                let fullName = author.firstName + ' ' + author.lastName;
                authorsString = authorsString.concat(fullName, ', ');
            });
            //trimming ', '
            authors.textContent = authorsString.slice(0, -2);
        }

        const grade = document.createElement('div');
        grade.classList.add('div-table-cell');
        grade.textContent = book.grade;

        row.appendChild(title);
        row.appendChild(authors);
        row.appendChild(grade);
        table.appendChild(row);
    });

    tableHeadingRow.appendChild(titleHeading);
    tableHeadingRow.appendChild(authorsHeading);
    tableHeadingRow.appendChild(gradesHeading);
    tableHeading.appendChild(tableHeadingRow);
    table.appendChild(tableHeading);
    page.appendChild(table);
    return page;
}

export default books;
