import {createInput, createLabelFor} from "../elements.js";
import {navigateTo} from "../router.js";

const addBook = async (params) => {

    document.title = "Add book";
    const page = document.createElement("div");
    page.setAttribute('id','errorPlaceholder');
    let table = document.createElement('div');
    table.classList.add('div-table');

    let row = document.createElement('div');
    row.classList.add('div-table-row');

    let formContainer = document.createElement('div');
    formContainer.classList.add('form');

    const authors = await fetch('/api/index.php?command=getAuthors')
        .then(response => response.json());

    await formContainer.appendChild(await createBookAddForm(authors, params));
    row.appendChild(formContainer);
    table.appendChild(row);
    page.appendChild(table);

    return page;
}

async function createBookAddForm(authorsToDisplay, params) {
    let form = document.createElement('form');

    form.appendChild(createLabelFor('title', 'Pealkiri:'));
    let title = createInput('text', 'title', 'title')
    form.appendChild(title);
    form.appendChild(document.createElement('br'));

    form.appendChild(createLabelFor('firstAuthor', 'Autor 1:'));
    let firstAuthorsSelect = createAuthorsSelect(authorsToDisplay, 'firstAuthor', 'firstAuthor');
    form.appendChild(firstAuthorsSelect);
    form.appendChild(document.createElement('br'));

    form.appendChild(createLabelFor('secondAuthor', 'Autor 2:'));
    let secondAuthorSelect = createAuthorsSelect(authorsToDisplay, 'secondAuthor', 'secondAuthor');
    form.appendChild(secondAuthorSelect);
    form.appendChild(document.createElement('br'));

    form.appendChild(createLabelFor('grade', 'Hinnang:'));
    for (let i = 1; i <= 5; i++) {
        let radioLabel = document.createElement('span');
        radioLabel.textContent = (i).toString();
        form.appendChild(radioLabel);

        let inputGradeRadio = document.createElement('input');
        inputGradeRadio.setAttribute('type', 'radio');
        inputGradeRadio.setAttribute('name', 'grade');
        inputGradeRadio.setAttribute('id', 'grade');
        inputGradeRadio.setAttribute('value', (i).toString());
        form.appendChild(inputGradeRadio);
    }
    form.appendChild(document.createElement('br'));

    let isRead = createInput('checkbox', 'isRead', 'isRead');
    form.appendChild(createLabelFor('isRead', 'Loetud:'));
    form.appendChild(isRead);

    let submitButton = createInput('button', 'submitButton', 'submitButton');
    submitButton.value = 'Salvesta';
    submitButton.addEventListener('click', async () => {
        const title = document.getElementById('title').value;
        const firstAuthorId = document.getElementById('firstAuthor').value;
        const secondAuthorId = document.getElementById('secondAuthor').value;
        const grade = document
            .querySelector('input[name=grade]:checked') != null ? document
            .querySelector('input[name=grade]:checked').value : '';
        const isRead = document.getElementById('isRead').checked;

        if (isBookTitleValid(title)){
            const book = {
                title,
                firstAuthorId,
                secondAuthorId,
                grade,
                isRead
            };

            if (params.get('bookId')){
                book['id'] = params.get('bookId');

                fetch(`/api/index.php?command=editBook`, {
                    method: "POST",
                    body: JSON.stringify(book)
                }).then(response => {
                    if (response.status === 205){
                        navigateTo('/');
                    }
                });

            } else {
                fetch('/api/index.php?command=addBook', {
                    method: "POST",
                    body: JSON.stringify(book)
                }).then(response => {
                    if (response.status === 201){
                        navigateTo('/?success=true');
                    }
                });
            }

        } else {
            if (document.getElementById('error-block') == null){
                let page = document.getElementById('errorPlaceholder');
                let form = page.lastChild;
                let errorBlock = document.createElement('div');
                errorBlock.classList.add('class','error-block');
                errorBlock.setAttribute('id', 'error-block');
                errorBlock.textContent = 'Pealkiri peab olema 3 kuni 23 märki!';
                page.insertBefore(errorBlock, form);
            }
        }


    });
    form.appendChild(document.createElement('br'));
    form.appendChild(submitButton);

    if (params.get('bookId')){
        let id = params.get('bookId');
        let book = await getBookById(id);

        title.value = book.title;
        if (book.authors.length > 0){
            firstAuthorsSelect.value = book.authors[0].id;
            if (book.authors[1] != null){
                secondAuthorSelect.value = book.authors[1].id;
            }
        }

        let collection = form.children;
        for (let item of collection){
            if (item.id === 'grade'){
                if (item.value === book.grade){
                    item.checked = true;
                }
            }
        }

        isRead.checked = parseInt(book.isRead);

        let deleteButton = createInput('button', 'deleteButton', 'deleteButton');
        deleteButton.value = 'Kustuta';
        form.appendChild(deleteButton);
        deleteButton.addEventListener('click', async () => {
            fetch(`/api/index.php?command=deleteBook&bookId=${encodeURIComponent(book.id)}`, {
                method : "GET"
            })
                .then(response => {
                    if (response.status === 205){
                        navigateTo('/');
                    }
                });
        });


    }
    return form;
}

function createAuthorsSelect(authors, name, id) {
    let selectElement = document.createElement('select');
    selectElement.setAttribute('name', name);
    selectElement.setAttribute('id', id);
    selectElement.add(document.createElement('option'));
    authors.forEach(author => {
        let newOption = document.createElement('option');
        let fullName = author.firstName + ' ' + author.lastName;
        newOption.textContent = fullName;
        newOption.value = author.id;
        selectElement.add(newOption);
    });
    return selectElement;
}

async function getBookById(id){
    const book = await fetch(`/api/index.php?command=getBookById&bookId=${encodeURIComponent(id)}`)
        .then(response => response.json());
    return book;
}

function isBookTitleValid(title){
    return title.length >= 3 && title.length <= 23;
}
export default addBook;