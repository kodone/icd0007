import books from './views/books.js';
import addBook from './views/addBook.js';

export const router = async () => {
    const routes = [
        { path: "/", renderView: books},
        { path: "/add-book", renderView: addBook }
    ];
    const defaultRoute = routes[0];
    const currentPath = location.pathname;
    const params = new URLSearchParams(location.search);

    const routeMatch = routes.find(route => route.path === currentPath);

    const content = document.getElementById("content");
    removeAllChildNodes(content);
    if (routeMatch) {
        const view = await routeMatch.renderView(params)
        content.appendChild(view);
    } else {
        const view = await defaultRoute.renderView(params);
        content.appendChild(view);
    }
}

const removeAllChildNodes = (element) => {
    while (element.hasChildNodes()) {
        element.removeChild(element.firstChild);
    }
}

export const navigateTo = (path) => {
    history.pushState(null, null, path);
    router();
}