export function createInput(inputType, name, id) {
    let input = document.createElement('input');
    input.setAttribute('type', inputType);
    input.setAttribute('name', name);
    input.setAttribute('id', id);
    return input;
}

export function createLabelFor(id, text) {
    let label = document.createElement('label');
    label.htmlFor = id;
    label.textContent = text;
    return label;
}
