async function add() {
    const title = "jolo";
    const grade = "2";
    const isRead = true;
    const id = '50';
    const firstAuthorId = 25;
    const secondAuthorId = 31;

    const book = {
        title,
        grade,
        isRead,
        id,
        firstAuthorId,
        secondAuthorId
    };

    await fetch(`/api/index.php?command=editBook`, {
        method: "POST",
        body: JSON.stringify(book)
    }).then(function (response) {
        if (response.status !== 205) {
            throw new Error("HTTP status " + response.status);
        } else {
            console.log("Success!")
        }
    });
}

async function getAllBooks() {
    await fetch('/api/index.php?command=getBooks', {
        method: "GET"
    }).then(response => response.json())
        .then(json => json.forEach(item => console.log(item)));

    console.log("SUCCESS!");
}