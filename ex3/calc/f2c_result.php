<?php

$temp = $_POST['temperature'] ?? '';

if (empty($temp)) {
    $message = 'Insert temperature';
} elseif (!is_numeric($temp)) {
    $message = 'Temperature must be an integer';
} else {
    $fahrenheit = fahrenheitToCelsius($temp);
    $message = "$temp decrees in Fahrenheit is $fahrenheit decrees in Celsius";
}

function fahrenheitToCelsius($temp): float
{
    return round(($temp - 32) / (9 / 5), 2);
}

?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Fahrenheit to Celsius</title>
</head>
<body>
<nav>
    <a href="index.html">Celsius to Fahrenheit</a> |
    <a href="f2c.html">Fahrenheit to Celsius</a>
</nav>
<main>
    <h3>Fahrenheit to Celsius</h3>

    <em><?= $message ?> </em> /<br>
</main>
</body>
</html>