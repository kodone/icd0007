<?php

include_once __DIR__ . '/Post.php';

const DATA_FILE = __DIR__ . '/data/posts.txt';

function getAllPosts(): array
{
    $posts = [];
    $lines = file(DATA_FILE);

    foreach ($lines as $line) {

        $post = explode(";", trim($line));  // receiving ENCODED text
        [$title, $text] = $post;
        array_push($posts, new Post(urldecode($title), urldecode($text)));
    }
    return $posts;
}

function savePost(Post $post): void
{
//    $string = urlencode($post->title . ";" . $post->text);
    $encodedTitle = urlencode($post->title);
    $encodedText = urlencode($post->text);
    $string = $encodedTitle . ";" . $encodedText;
    file_put_contents(DATA_FILE, $string . PHP_EOL, FILE_APPEND);

}

function printPosts(array $posts)
{
    foreach ($posts as $post) {
        print $post . PHP_EOL;
    }
}