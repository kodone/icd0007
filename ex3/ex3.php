<?php

$numbers = [1, 2, 5, 6, 2, 11, 2, 7];

function getOddNumbers($list): array
{
    $oddNumbers = [];
    foreach ($list as $item){
        if ($item % 2 === 1){
            array_push($oddNumbers, $item);
        }
    }
    return $oddNumbers;
}

print_r(getOddNumbers($numbers));