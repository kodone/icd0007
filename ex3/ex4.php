<?php
for ($i = 1; $i <= 15; $i++){
    if ($i % 3 === 0){
        print "Fizz\n";
    } elseif ($i % 5 === 0) {
        print "Bizz\n";
    } elseif ($i % 5 === 0 && $i % 3 === 0){
        print "FizzBizz\n";
    } else {
        print $i . "\n";
    }
}

